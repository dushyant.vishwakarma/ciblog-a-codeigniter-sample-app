<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category_Model extends CI_Model{

    public function __construct(){
        $this->load->database();
    }

    public function create_category(){
        $data = [
            'name' => $this->input->post('name')
        ];

        $result = $this->db->insert('categories',$data);

        if($result)
            return true;
        else
            return false;
    }

    public function get_category($id){
        $query = $this->db->get_where('categories',['id' => $id]);

        return $query->row();
    }

}