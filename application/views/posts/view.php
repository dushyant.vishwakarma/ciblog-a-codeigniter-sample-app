<h2><?= $post['title'] ?></h2>
<small class="post-date">Posted On: <?php echo $post['created_at']; ?></small>

<img src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']?>" alt="Post Image" />

<div class="posts-body">
    <?php echo $post['body'] ?>
</div>

<div class="btn-group">
    <a class="btn btn-secondary" href="<?php echo base_url() ?>posts/edit/<?php echo $post['slug']?>">Edit</a>

    <?php echo form_open('posts/delete/'.$post['id']); ?>
        <input type="submit" value="Delete" class="btn btn-danger" />
    <?php echo form_close(); ?>
</div>
