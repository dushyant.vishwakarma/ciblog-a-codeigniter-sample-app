<h2>
    <?php if(isset($title->name)){ 
        echo $title->name;
    } else {
        echo "Latest Posts";
    }
    ?>
</h2>

<?php if($this->session->flashdata('post_created') ): ?>
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo $this->session->flashdata('post_created'); ?>
    </div>
<?php endif; ?>

<?php if($this->session->flashdata('post_deleted') ): ?>
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo $this->session->flashdata('post_deleted'); ?>
    </div>
<?php endif; ?>

<?php if($this->session->flashdata('post_updated') ): ?>
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo $this->session->flashdata('post_updated'); ?>
    </div>
<?php endif; ?>

<?php foreach($posts as $post): ?>
    <h3><?php echo $post['title']; ?></h3>
    <div class="row">
        <div class="col-md-3">
            <img class="post-thumbnail" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']?>" alt="Post Image" />
        </div>
        
        <div class="col-md-9">
            <small class="post-date">Posted On: <?php echo $post['created_at']; ?> in <strong><?php echo $post['name']?></strong></small>
            <p><?php echo word_limiter($post['body'],60); ?></p>
            <p><a class="btn btn-outline-primary" href="<?php echo site_url("/posts/" . $post['slug']); ?>">Read More</a></p>
        </div>
    </div>
<?php endforeach; ?>
