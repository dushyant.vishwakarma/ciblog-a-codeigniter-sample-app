<h2><?= $title ?></h2>

<?php echo validation_errors(); ?>

<!-- Create Post Form -->
<?php echo form_open('posts/update'); ?>

  <input type="hidden" name="id" value="<?= $post['id']?>">

  <input type="hidden" name="slug" value="<?= $post['slug']?>">

  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="title" value="<?= $post['title'] ?>" placeholder="Add Title">
  </div>

  <div class="form-group">
    <label>Body</label>
    <textarea class="form-control" id="editor" name="body" placeholder="Add Body" id="" cols="30" rows="10"><?= $post['body'] ?></textarea> 
 </div>

 <div class="form-group">
    <label>Category</label>
    <select name="category_id" class="form-control">
      <?php foreach($categories as $category): ?>
          <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
      <?php endforeach; ?> 
    </select> 
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
<?php echo form_close(); ?>