<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posts extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("Post_Model");
    }
    public function index(){

        $data['title'] = "Lastest Posts";

        $data['posts'] = $this->Post_Model->get_posts();

        // print_r($data['posts']);exit;

        $this->load->view("templates/header");
        $this->load->view("posts/index",$data);
        $this->load->view("templates/footer");
    }

    public function view($slug = NULL){
        $data['post'] = $this->Post_Model->get_posts($slug);

        if(empty($data['post'])){
            show_404();
        }

        $data['title'] = $data['post']['title'];

        $this->load->view("templates/header");
        $this->load->view("posts/view",$data);
        $this->load->view("templates/footer");
    }

    public function create(){

        $data['title'] = "Create Post";

        $data['categories'] = $this->Post_Model->get_categories();

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('body','Body','required');

        if($this->form_validation->run() === FALSE){
            $this->load->view("templates/header");
            $this->load->view("posts/create",$data);
            $this->load->view("templates/footer");    
        } else {

            // Upload Image
            $config['upload_path'] = './assets/images/posts';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size'] = '2048';
            $config['max_width'] = '500';
            $config['height'] = '500';

            $this->load->library('upload',$config);

            if(!$this->upload->do_upload()){
                $errors = ['error' => $this->upload->display_errors()];
                // echo "<pre>";print_r($errors);exit;
                $post_image = 'no_image.png';
            } else {
                $data = ['upload_data' => $this->upload->data()];
                $post_image = $_FILES['userfile']['name'];
            }
            // End Upload Image

            $save = $this->Post_Model->create_post($post_image);

            if($save == TRUE){
                $this->session->set_flashdata('post_created','Post Created Successfully');
                redirect('posts');
            }
        }

       
    }

    public function delete($id){

        $delete = $this->Post_Model->delete_post($id);

        if($delete == true){
            $this->session->set_flashdata('post_deleted','Post Deleted Successfully.');
            redirect('posts');
        }

    }

    public function edit($slug){

        $data['post'] = $this->Post_Model->get_posts($slug);

        $data['categories'] = $this->Post_Model->get_categories();


        if(empty($data['post'])){
            show_404();
        }

        $data['title'] = 'Edit Post';

        $this->load->view("templates/header");
        $this->load->view("posts/edit",$data);
        $this->load->view("templates/footer");



    }

    public function update(){
        $id = $this->input->post('id');
        $slug =  $this->input->post('slug');
        $update = $this->Post_Model->update_post($id);

        if($update == true){
            $this->session->set_flashdata('post_updated','Post Updated Successfully.');
            redirect('posts');
        }
    }
}