<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model("Category_Model");
        $this->load->model("Post_Model");

    }

    public function index(){
        $data['title'] = "Categories";

        $data['categories'] = $this->Post_Model->get_categories();

        $this->load->view('templates/header');
        $this->load->view('categories/index',$data);
        $this->load->view('templates/footer');


    }

    public function create(){
        $data['title'] = "Create Category";

        $this->form_validation->set_rules('name','Name','required');

        if($this->form_validation->run() === FALSE){

            $this->load->view('templates/header');
            $this->load->view('categories/create',$data);
            $this->load->view('templates/footer');
        } else {
            $save = $this->Category_Model->create_category();

            if($save == TRUE){
                $this->session->set_flashdata('category_created','Category Created Successfully');
                redirect('categories');
            }
        }
    } 

    public function posts($id){
        $data['title'] = $this->Category_Model->get_category($id);

        $data['posts'] = $this->Post_Model->get_post_by_category($id);

        $this->load->view('templates/header');
        $this->load->view('posts/index',$data);
        $this->load->view('templates/footer');
    }
}