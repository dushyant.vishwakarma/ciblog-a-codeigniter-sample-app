CREATE TABLE `ciblog`.`posts` (
     `id` INT NOT NULL AUTO_INCREMENT , 
     `title` VARCHAR(255) NOT NULL , 
     `slug` VARCHAR(255) NOT NULL , 
     `body` TEXT NOT NULL , 
     `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB; 

CREATE TABLE `ciblog`.`categories` ( 
    `id` INT(11) NOT NULL AUTO_INCREMENT , 
    `name` VARCHAR(255) NOT NULL , 
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `posts` ADD `category_id` INT(11) NOT NULL AFTER `id`;

ALTER TABLE `posts` ADD `post_image` VARCHAR(255) NOT NULL AFTER `body`;





